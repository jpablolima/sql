#codigo de criaação de sql concluido


# criação das tabela socio
create table socio(
id_socio integer  not null auto_increment,
nome varchar(256) not null,
cpf varchar(11),
email varchar(256),
primary key (id_socio)

);

# criação das tabela socio
create table situacao(
id_situacao integer not null auto_increment,
situacao varchar(10),
primary key(id_situacao)
);

# criação das tabela marca
create table marca(
id_marca integer not null auto_increment,
marca varchar(128),
primary key(id_marca)

);


# criação das tabela carro
create table carro (
id_carro integer not null auto_increment,
modelo varchar(128),
cor varchar(64),
placa varchar(10),
id_socio integer,
primary key(id_carro)

);
#realizando alteração nas tebelas
alter table socio add id_situacao integer;
alter table carro add id_marca integer;
alter table marca add id_socio integer;

#alteração nas tabelas para criação de Constraint Foreign Key
alter table socio
add foreign key (id_situacao)
references socio(id_socio);

alter table carro 
add foreign key (id_socio)
references carro(id_carro);

alter table carro
add foreign key (id_marca)
references carro(id_carro);


describe socio;
#drop database concessionaria;